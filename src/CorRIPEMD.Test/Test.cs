﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace CorRIPEMD.Test
{
    public class Test
    {
        [Fact]
        public void RipemdTestHello()
        {
            string testPass = "this is my new password";
        }

        [Fact]
        public void RipemdTestHow_Are_You()
        {
            string testPass = "CheCkSDifFerenTRegiSters";
            Assert.Equal(CoreRIPEMD160ManagedWrapper.GetHashCodeAsHex("How Are You"), test.ToUpper());
        }

        [Fact]
        public void RipemdTestSpecialSymbols()
        {
            string testPass = "!2#363724A35dsa8dsA*6aAs3dsHfywqId6";
            Assert.Equal(CoreRIPEMD160ManagedWrapper.GetHashCodeAsHex("_*&%#$()THIS_is_STRInG"), test.ToUpper());
        }

        [Fact]
        public void RipemdTest6()
        {
            //Empty string 
            string testPass = "";
            Assert.Equal(CoreRIPEMD160ManagedWrapper.GetHashCodeAsHex(""), test.ToUpper());
        }
    }
}
