﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace CorRIPEMD
{
    public class Program
    {
        public static void Main(string[] args)
        {
            string testPass = "Hi I am NiCk";
            Console.WriteLine($"test password is: {testPass}");

            string salt = EncryptTools.GenerateSalt();
            Console.WriteLine($"salt is: {salt}");
            string resultHash = EncryptTools.GenerateFullPassword(testPass, salt);
            Console.WriteLine($"Result is: {resultHash}");

            bool validationResult = EncryptTools.VerifyPassword("Hi I am NiCk", salt, resultHash);
            Console.WriteLine($"Validation res: {validationResult}");

            Console.ReadLine();
        }
    }
}
