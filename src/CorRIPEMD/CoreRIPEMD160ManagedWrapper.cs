﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CorRIPEMD
{
    public static class CoreRIPEMD160ManagedWrapper
    {
        //construct
        static CoreRIPEMD160ManagedWrapper()
        {
            internalRIPEMDManager = new CoreRIPEMD160Managed();
        }

        //interfaces
        public static string GetHashCodeAsHex(string inputString)
        {
            return GetHashCodeAsHex(inputString, Encoding.UTF8);
        }
        public static string GetHashCodeAsHex(string inputString, Encoding encoding)
        {
            byte[] hashFuncResult = internalRIPEMDManager.ComputeHash( encoding.GetBytes(inputString) );
            

            StringBuilder resultString = new StringBuilder();
            foreach (var bt in hashFuncResult)
            {
                resultString.Append($"{bt:X2}");
            }

            return resultString.ToString();
        }

        public static string GetHashCodeAsDec(string inputString)
        {
            byte[] hashFuncResult = internalRIPEMDManager.ComputeHash(System.Text.Encoding.UTF8.GetBytes(inputString));

            StringBuilder resultString = new StringBuilder();
            foreach (var bt in hashFuncResult)
            {
                resultString.Append($"{bt}");
            }

            return resultString.ToString();
        }

        private static CoreRIPEMD160Managed internalRIPEMDManager;
    }
}
