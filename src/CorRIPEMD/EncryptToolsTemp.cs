﻿namespace CorRIPEMD
{
    using System;
    using System.Security.Cryptography;
    using System.Text;

    public class EncryptTools
    {
        private static readonly CoreRIPEMD160Managed innerRMD160Coder;        

        static EncryptTools()
        {
            innerRMD160Coder = new CoreRIPEMD160Managed();
        }

        public static string GenerateFullPassword(string openPassword, string saltAsBase64Str)
        {
            //convert openPassword to byte array with utf8 encoder
            //use byte array as argument for ripemd algo
            byte[] hashResult = innerRMD160Coder.ComputeHash(Encoding.UTF8.GetBytes(openPassword));

            //generate salt and assign its string representation to out parameter
            byte[] saltAsBytes = Convert.FromBase64String(saltAsBase64Str);

            //make full password as byte array
            byte[] fullPassAsBytes = new byte[hashResult.Length + saltAsBytes.Length];
            hashResult.CopyTo(fullPassAsBytes, 0);
            saltAsBytes.CopyTo(fullPassAsBytes, hashResult.Length);

            //use concatenation result as argument from sha algo
            var shaEncoder = SHA512.Create();
            byte[] result = shaEncoder.ComputeHash(fullPassAsBytes);

            //return sha result as base64 string
            return Convert.ToBase64String(result);
        }
        
        //returns base64 string as unique salt
        public static string GenerateSalt()
        {
            //array to be filled with rand bytes by cryptogenerator
            //should be divisible by 3 to eliminate dummychars
            byte[] randBytesForSalt = new byte[18];
            //create cryptogenerator
            RandomNumberGenerator randGenerator = RandomNumberGenerator.Create();
            //fill array with rand bytes
            randGenerator.GetBytes(randBytesForSalt);

            return Convert.ToBase64String(randBytesForSalt);
        }

        public static bool VerifyPassword(string openPassword, string salt, string passwordHash)
        {
            string result = GenerateFullPassword(openPassword, salt);
            Console.WriteLine($"2nd hash count res: {result}");
            return string.Equals(passwordHash, result);
        }
    }
}
